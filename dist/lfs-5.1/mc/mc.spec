%define pfx /opt/freescale/rootfs/%{_target_cpu}
%define mc_version 4.8.10
%define mc_release 1
%define mc_tarball 4.8.10

Summary         : User-friendly text console file manager and visual shell
Name            : mc
Version         : %{mc_version}
Release         : %{mc_release}
Epoch           : 3
License         : GPL3+
Group           : System Environment/Shells
URL             : http://www.midnight-commander.org/
Source0         : %{name}-%{mc_tarball}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
GNU Midnight Commander is a visual file manager. It's a feature rich
full-screen text mode application that allows you to copy, move and
delete files and whole directory trees, search for files and run
commands in the subshell. Internal viewer and editor are included. Mouse
is supported on Linux console. VFS (Virtual Filesystem) allows you to
view archives and files on remote servers (via SAMBA, FTP or SSH).

%Prep
%setup

%Build
./configure \
	--prefix=${_prefix} --host=$CFGHOST --build=${_build} \
	--with-screen=ncurses \
	--enable-charset \
	--without-x \
	--disable-gtk-doc \
	--enable-static \
	--disable-shared \
	--disable-doxygen-doc
#	--enable-vfs-smb 
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT


%Files
%defattr(-,root,root)
%{pfx}/*
