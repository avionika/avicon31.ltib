%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : Samba is useful for creating and connecting to Windows shares using the SMB protocol
Name            : samba
Version         : 3.6.22
Release         : 1
License         : GPL
Vendor          : Freescale
Packager        : Michael Reiss
Group           : System Environment/Daemons
Source          : %{name}-%{version}.tar.gz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

%Prep
%setup

%Build
#./configure --prefix=/ --cross-compile --target=$CFGHOST --with-sendfile-support --disable-cups --with-privatedir=/tmp
cd source3
export samba_cv_CC_NEGATIVE_ENUM_VALUES="yes"
export libreplace_cv_HAVE_GETADDRINFO="no"
export ac_cv_file__proc_sys_kernel_core_pattern="yes"
export samba_cv_USE_SETRESUID="yes"
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build} --target=$CFGHOST --with-sendfile-support --disable-cups
make

%Install
rm -rf $RPM_BUILD_ROOT
cd source3
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
