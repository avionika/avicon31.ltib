%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : Qt
Name            : qt-everywhere-opensource-src
Version         : 5.1.1
Release         : 0
License         : GNU GPL
Vendor          : Freescale
Packager        : Sergey Savchuk
Group           : System Environment/Libraries
Source          : %{name}-%{version}.tar.gz
Patch0          : qt-everywhere-opensource-src-5.1.1.linuxfb_virtualscreen.patch
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
URL             : http://download.qt-project.org/official_releases/qt/5.1/5.1.1/single/%{name}-%{version}.tar.gz
%Description
%{summary}

%Prep
%setup
%patch0 -p1

#Creating mkspec

initscriptpath=qtbase/mkspecs/devices/linux-imx51-g++
mkdir -p $initscriptpath
initscript=$initscriptpath/qmake.conf
cat > $initscript << EOF
#
# qmake configuration for the Freescale iMX53 board
#
# This mkspec is based and tested on the Yocto Project's Poky Distribution
# with libEGL.so from Freescale without the X11 dependency.
#
# This mkspec is tested with a framebuffer (eglfs) configuration
#
# A typical configure line looks like:
#
# export PATH=/opt/freescale/usr/local/gcc-4.4.4-glibc-2.11.1-multilib-1.0/arm-fsl-linux-gnueabi/bin:${PATH}
#
# ./configure \\
#    -prefix /usr/local/qt-5.1.1 \\
#    -hostprefix /usr/local/5t-5.1.1-arm51-fsl-linux-gnueabi \\
#    -release -device linux-imx51-g++ \\
#    -opensource -confirm-license \\
#    -device-option CROSS_COMPILE=arm-fsl-linux-gnueabi- \\
#    -sysroot /opt/ltib/rootfs \\
#    -no-gif -no-libjpeg -no-openssl -no-iconv -no-pch -no-xcb -no-dbus -no-wayland -no-cups -no-gtkstyle \\
#    -opengl es2 -eglfs \\
#    -make libs \\
#    -v

include(../common/linux_device_pre.conf)

QMAKE_INCDIR            += \$\$[QT_SYSROOT]/usr/include
QMAKE_LIBDIR            += \$\$[QT_SYSROOT]/usr/lib

QMAKE_LIBS_EGL          += -lEGL
QMAKE_LIBS_OPENGL       = -lGLU -lGL
QMAKE_LIBS_OPENGL_QT    = -lGL
QMAKE_LIBS_OPENGL_ES1   = -lGLESv1_CM
QMAKE_LIBS_OPENGL_ES1CL = -lGLES_CL
QMAKE_LIBS_OPENGL_ES2   += -lGLESv2 -lEGL
QMAKE_LIBS_OPENVG       += -lOpenVG -lEGL

SYSROOT = /opt/ltib/rootfs

QMAKE_INCDIR_OPENGL       = \$\$SYSROOT/usr/include/GL
QMAKE_LIBDIR_OPENGL       = \$\$SYSROOT/usr/lib
QMAKE_INCDIR_POWERVR      = \$\$SYSROOT/usr/include
QMAKE_INCDIR_OPENGL_ES1   = \$\$SYSROOT/usr/include/GLES
QMAKE_LIBDIR_OPENGL_ES1   = \$\$SYSROOT/usr/lib
QMAKE_INCDIR_OPENGL_ES1CL = \$\$SYSROOT/usr/include/GLES
QMAKE_LIBDIR_OPENGL_ES1CL = \$\$SYSROOT/usr/lib
QMAKE_INCDIR_OPENGL_ES2   = \$\$SYSROOT/usr/include/GLES2
QMAKE_LIBDIR_OPENGL_ES2   = \$\$SYSROOT/usr/lib
QMAKE_INCDIR_EGL          = \$\$SYSROOT/usr/include/EGL
QMAKE_LIBDIR_EGL          = \$\$SYSROOT/usr/lib
QMAKE_INCDIR_OPENVG       = \$\$SYSROOT/usr/include/VG
QMAKE_LIBDIR_OPENVG       = \$\$SYSROOT/usr/lib

QMAKE_LFLAGS            += -Wl,-rpath-link,\$\$[QT_SYSROOT]/usr/lib

IMX5_CFLAGS             = -march=armv7-a -mfpu=vfpv3 -mfloat-abi=softfp -DLINUX=1 -DEGL_API_FB=1 -Wno-psabi
IMX5_CFLAGS_RELEASE     = -O2 \$\$IMX5_CFLAGS

QMAKE_CFLAGS_RELEASE    += \$\$IMX5_CFLAGS_RELEASE
QMAKE_CXXFLAGS_RELEASE  += \$\$IMX5_CFLAGS_RELEASE
QMAKE_CFLAGS_DEBUG      += \$\$IMX5_CFLAGS
QMAKE_CXXFLAGS_DEBUG    += \$\$IMX5_CFLAGS

include(../common/linux_device_post.conf)

load(qt_config)

EOF

chmod 744 $initscript

initscript=$initscriptpath/qplatformdefs.h
cat > $initscript << EOF
#include "../../linux-g++/qplatformdefs.h"
EOF

chmod 744 $initscript

%Build

export PATH=$UNSPOOF_PATH

# Unset compiler to prevent gcc being used when the cross
unset CC CXX LD

# Unset PLATFORM because this is used as host machine.
unset PLATFORM

#    -prefix $RPM_BUILD_ROOT%{pfx}/usr/local \

# -no-icu ............ Do not compile support for ICU (International Components for Unicode) libraries.
./configure \
    -prefix /usr/local/qt-%{version} \
    -hostprefix /usr/local/qt-%{version}-host \
    -debug -device linux-imx51-g++ \
    -opensource -confirm-license \
    -device-option CROSS_COMPILE=${TOOLCHAIN_PREFIX} \
    -sysroot /opt/ltib/rootfs \
    -no-gif -no-libjpeg -no-openssl -no-iconv -no-pch -no-dbus -no-wayland -no-cups -no-gtkstyle \
    -opengl es2 -eglfs \
    -make libs \
    -v \
    -tslib

CPU_CORES_COUNT=1
if [ -a /proc/cpuinfo ]; then
    CPU_CORES_COUNT=$(expr $(grep 'processor' /proc/cpuinfo | wc -l) + 1)
fi
echo "CPU_CORES_COUNT=$CPU_CORES_COUNT"

make -j $CPU_CORES_COUNT && echo '-- build successful'

%Install

export PATH=$UNSPOOF_PATH

make install INSTALL_ROOT=$RPM_BUILD_ROOT%{pfx}
# it's must to be done because qmake adds to prefix sysroot
mv $RPM_BUILD_ROOT%{pfx}/opt/ltib/rootfs/usr/local/* -t $RPM_BUILD_ROOT%{pfx}/usr/local/
rm -Rf $RPM_BUILD_ROOT%{pfx}/opt

export PATH=$SPOOF_PATH

%Clean
#rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/usr/local/qt-%{version}/*
