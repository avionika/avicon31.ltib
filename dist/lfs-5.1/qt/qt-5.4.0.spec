%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : Qt
Name            : qt-everywhere-opensource-src
Version         : 5.4.0
Release         : 0
License         : GNU GPL
Vendor          : Freescale
Packager        : Sergey Savchuk
Group           : System Environment/Libraries
Source          : %{name}-%{version}.tar.gz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
Patch0          : qt-everywhere-opensource-src-5.4.0.imx51.patch
URL             : http://master.qt.io/official_releases/qt/5.4/%{version}/%{name}-%{version}.tar.gz
%Description
%{summary}

%Prep
%setup
%patch0 -p1

%Build

export PATH=$UNSPOOF_PATH

# Unset compiler to prevent gcc being used when the cross
unset CC CXX LD

# Unset PLATFORM because this is used as host machine.
unset PLATFORM

./configure \
    -prefix /usr/local/qt-%{version} \
    -hostprefix /usr/local/qt-%{version}-host \
    -debug -device linux-imx51-g++ \
    -opensource -confirm-license \
    -device-option CROSS_COMPILE=${TOOLCHAIN_PREFIX} \
    -sysroot /opt/ltib/rootfs \
    -no-gif -no-libjpeg -no-openssl -no-iconv -no-pch -no-dbus -no-cups -no-gtkstyle -no-kms \
    -no-opengl -no-eglfs \
    -make libs \
    -nomake examples -nomake tests \
    -no-use-gold-linker \
    -no-qml-debug \
    -skip qtactiveqt \
    -skip qtenginio \
    -skip qtquick1 \
    -skip qtquickcontrols \
    -skip qtscript \
    -skip qtwebkit \
    -skip qtwebsockets \
    -skip qtxmlpatterns \
    -v \
    -tslib

CPU_CORES_COUNT=1
if [ -a /proc/cpuinfo ]; then
    CPU_CORES_COUNT=$(expr $(grep 'processor' /proc/cpuinfo | wc -l) + 1)
fi
echo "CPU_CORES_COUNT=$CPU_CORES_COUNT"

make -j $CPU_CORES_COUNT && echo '-- build successful'

%Install

export PATH=$UNSPOOF_PATH

make install INSTALL_ROOT=$RPM_BUILD_ROOT%{pfx}
# it's must to be done because qmake adds to prefix sysroot
mv -f $RPM_BUILD_ROOT%{pfx}/opt/ltib/rootfs/usr/local/* -t $RPM_BUILD_ROOT%{pfx}/usr/local/
rm -Rf $RPM_BUILD_ROOT%{pfx}/opt

export PATH=$SPOOF_PATH

%Clean
#rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/usr/local/qt-%{version}/*
