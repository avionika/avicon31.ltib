%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary        : International Components for Unicode
Name           : icu4c
Version        : 52_1
Release        : 1
Packager       : Sergey Savchuk
Copyright      : X License
Group          : System Environment/Libraries
Source         : %{name}-%{version}-src.tgz
BuildRoot      : %{_tmppath}/%{name}
Prefix         : %{pfx}
%description
ICU is a set of C and C++ libraries that provides robust and full-featured
Unicode and locale support. The library provides calendar support, conversions
for many character sets, language sensitive collation, date
and time formatting, support for many locales, message catalogs
and resources, message formatting, normalization, number and currency
formatting, time zones support, transliteration, word, line and
sentence breaking, etc.

This package contains the Unicode character database and derived
properties, along with converters and time zones data.

This package contains the runtime libraries for ICU. It does
not contain any of the data files needed at runtime and present in the
`icu' and `icu-locales` packages.

%Prep
%setup -q -n icu

%build

CPU_CORES_COUNT=1
if [ -a /proc/cpuinfo ]; then
    CPU_CORES_COUNT=$(expr $(grep 'processor' /proc/cpuinfo | wc -l) + 1)
fi
echo "CPU_CORES_COUNT=$CPU_CORES_COUNT"

export PATH=$UNSPOOF_PATH

mkdir -p host
cd host
../source/runConfigureICU Linux/gcc
make -j $CPU_CORES_COUNT

export PATH=$SPOOF_PATH

cd ..
mkdir -p target
cd target
../source/configure --prefix=/usr --host=$CFGHOST --build=%{_build} --with-cross-build=$RPM_BUILD_DIR/icu/host --enable-shared --enable-static --disable-samples
make -j $CPU_CORES_COUNT

%install

cd target
make install DESTDIR=$RPM_BUILD_ROOT%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{pfx}/*