%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : A library of functions used by GDK, GTK+, and many applications
Name            : glib2
Version         : 2.30.2
Release         : 0
License         : LGPL
Vendor          : Freescale
Packager        : Stuart Hughes/Kurt Mahan
Group           : System Environment/Libraries
Source          : glib-%{version}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
URL             : http://ftp.gnome.org/pub/gnome/sources/%{name}-%{version}.tar.bz2

%Description
%{summary}

%Prep
%setup -n glib-%{version}

%Build
# prevent configure from trying to compile and
# run test binaries for the target.
lt_cv_path_NM=nm \
glib_cv_stack_grows=no \
glib_cv_uscore=no \
ac_cv_func_posix_getpwuid_r=yes \
ac_cv_func_posix_getgrgid_r=yes \
ac_cv_func_qsort_r=no \
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build}
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}
find $RPM_BUILD_ROOT/%{pfx}/%{_prefix}/lib -name "*.la" | xargs rm -f

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*

