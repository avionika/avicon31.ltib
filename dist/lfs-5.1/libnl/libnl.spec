%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : The libnl suite is a collection of libraries providing APIs to netlink protocol based Linux kernel interfaces. 
Name            : libnl
Version         : 1.1.4
Release         : 1
License         : LGPL
Vendor          : Radioavionika
Packager        : sergey Savchuk
Group           : System/Libraries
Source          : %{name}-%{version}.tar.gz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
URL             : http://people.suug.ch/~tgr/libnl/

%Description
%{summary}

%Prep
%setup

%Build
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build}
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
