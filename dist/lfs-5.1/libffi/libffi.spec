%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : The libffi library provides a portable, high level programming interface to various calling conventions. 
Name            : libffi
Version         : 3.0.13
Release         : 0
License         : BSD
Vendor          : Freescale
Packager        : Andre Silva
Group           : System Environment/Libraries
URL             : http://www.linuxfromscratch.org/blfs/view/svn/general/libffi.html
Source          : ftp://sourceware.org/pub/libffi/%{name}-%{version}.tar.gz
Source1         : http://www.mirrorservice.org/sites/sourceware.org/pub/libffi/%{name}-%{version}.tar.gz
Patch0          : libffi-3.0.13.includedir.patch
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

%Prep
%setup
%patch0 -p1

%Build
./configure --prefix=%{_prefix} --exec-prefix=%{_prefix} --host=$CFGHOST --build=%{_build} --disable-static
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}
find $RPM_BUILD_ROOT/%{pfx}/%{_prefix}/lib/ -name "*.la" | xargs rm -f

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
