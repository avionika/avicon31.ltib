%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : A cross-platform multimedia library.
Name            : SDL_ttf
Version         : 2.0.11
Release         : 1
Source          : http://www.libsdl.org/projects/SDL_ttf/release/%{name}-%{version}.tar.gz
URL             : http://www.libsdl.org/
License         : LGPL
Group           : System Environment/Libraries
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}


%Description
This is a sample library which allows you to use TrueType fonts in your SDL
applications. It comes with an example program "showfont" which displays an
example string for a given TrueType font file.

%Prep
%setup

%Build
./configure --host=$CFGHOST --build=%{_build} --enable-cross-compile --prefix=%{_prefix} --mandir=%{_mandir} --disable-esd

perl -pi -e 's,^sys_lib_search_path_spec=.*,sys_lib_search_path_spec=,' libtool

make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*

