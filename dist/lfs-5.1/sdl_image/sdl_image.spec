%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : A cross-platform multimedia library.
Name            : SDL_image
Version         : 1.2.12
Release         : 1
Source          : http://www.libsdl.org/projects/SDL_image/release/%{name}-%{version}.tar.gz
URL             : http://www.libsdl.org/
License         : LGPL
Group           : System Environment/Libraries
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}


%Description
SDL_image is an image file loading library.
It loads images as SDL surfaces and textures, and supports the following formats:
BMP, GIF, JPEG, LBM, PCX, PNG, PNM, TGA, TIFF, WEBP, XCF, XPM, XV 

%Prep
%setup

%Build
./configure --host=$CFGHOST --build=%{_build} --enable-cross-compile --prefix=%{_prefix} --mandir=%{_mandir} --disable-esd

perl -pi -e 's,^sys_lib_search_path_spec=.*,sys_lib_search_path_spec=,' libtool

make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*

