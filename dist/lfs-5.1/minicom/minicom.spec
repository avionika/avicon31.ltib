%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : TTY mode communications package ala Telix
Name            : minicom
Version         : 2.6.2
Release         : 1
License         : GPL
Vendor          : Freescale
Packager        : Umesh Nerlige
Group           : Applications/Communications
URL             : https://alioth.debian.org/frs/download.php/file/3869/minicom-2.6.2.tar.gz
Source          : %{name}-%{version}.tar.gz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

%Prep
%setup

%Build
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build}
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
