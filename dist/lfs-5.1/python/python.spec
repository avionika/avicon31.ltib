%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : An interpreted object-oriented programming language.
Name            : python
Version         : 2.7.2
Release         : 1
License         : OSI Approved Python License
Vendor          : Freescale
Packager        : Stuart Hughes
Group           : Development/Languages
Source          : Python-%{version}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
Patch1          : Python-%{version}-xcompile.patch

%Description
%{summary}

%Prep
%setup -n Python-%{version}
%patch1 -p1

%Build
#ORIG_PATH=$PATH
#export PATH=$UNSPOOF_PATH
#./configure
#make python Parser/pgen
#mv python hostpython
#mv Parser/pgen Parser/hostpgen
#make distclean
#export PATH=$ORIG_PATH

#test if there is ltib installed python
if [ ! -e $DEFPFX/usr/bin/python ]; then
  ORIG_PATH=$PATH
  export PATH=$UNSPOOF_PATH
  ./configure
  make python Parser/pgen
  mv python hostpython
  mv Parser/pgen Parser/hostpgen

  make distclean

  export PATH=$ORIG_PATH

  HOSTPYTHON=./hostpython
  HOSTPGEN=./Parser/hostpgen
else
  HOSTPYTHON=python
  HOSTPGEN=pgen
fi

CONFIG_SITE=config.site
export LDFLAGS="-L$DEV_IMAGE/usr/lib -L$RPM_BUILD_ROOT/%{pfx}/%{_prefix}/lib"
export CFLAGS="-I$DEV_IMAGE/usr/include"
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build} --enable-shared --without-libdb --without-ssl --disable-ipv6 --without-tcl --without-tk --without-curses  --without-ncurses
#make HOSTPYTHON=./hostpython  HOSTPGEN=./Parser/hostpgen CROSS_COMPILE=yes
make HOSTPYTHON=$HOSTPYTHON HOSTPGEN=$HOSTPYGEN CROSS_COMPILE=yes

%Install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{pfx}/%{_prefix}
make prefix=$RPM_BUILD_ROOT/%{pfx}/%{_prefix} HOSTPYTHON=./hostpython \
     CROSS_COMPILE=yes install  

%Clean
rm -rf $RPM_BUILD_ROOT


%Files
%defattr(-,root,root)
%{pfx}/*
