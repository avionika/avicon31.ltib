%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : EIM kernel module for test purposes
Name            : eim-tst-prosoft
Version         : 1.0
Release         : 0
License         : GPL
Vendor          : Prosoft-Spb
Packager        : xxxx
Group           : xxxx
URL             : http://xxxx
Source          : %{name}-%{version}.tgz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

%Prep
%setup 

%Build
KERNELDIR="$PWD/../linux-2.6.35.3"
KBUILD_OUTPUT="$KERNELDIR"

#Build module
make KBUILD_OUTPUT=$KBUILD_OUTPUT LINUXPATH=$KERNELDIR
# make ARCH=$LINTARCH CROSS_COMPILE= HOSTCC="$BUILDCC"

%Install
KERNELDIR="$PWD/../linux-2.6.35.3"
KBUILD_OUTPUT="$KERNELDIR"

rm -rf $RPM_BUILD_ROOT
make -j1 LINUXPATH=$KERNELDIR KBUILD_OUTPUT=$KBUILD_OUTPUT \
     DEPMOD=/bin/true INSTALL_MOD_PATH=$RPM_BUILD_ROOT/%{pfx} install

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(755,root,root)
%{pfx}/*
