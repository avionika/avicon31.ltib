%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : FFmpeg video and sound codecs and utilities.
Name            : ffmpeg
Version         : 2.5.3
Release         : 1
License         : LGPL
Vendor          : Radioavionika
Packager        : Savchuk Sergey
Group           : Applications
Source          : %{name}-%{version}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
URL             : http://ffmpeg.org/releases/ffmpeg-2.5.3.tar.bz2

%Description
%{summary}

%Prep
%setup

%Build
./configure --arch=$LINTARCH --disable-altivec --disable-mmx --disable-mmx2 --enable-cross-compile --prefix=%{_prefix} --mandir=%{_mandir} --extra-cflags=-I../linux/include --enable-shared --disable-static
make

%Install
rm -rf $RPM_BUILD_ROOT
make install INSTALL=cp DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Post
echo "%{_prefix}/usr/local/lib" >> %{_prefix}/etc/ld.so.conf
%{_prefix}/sbin/ldconfig

%Files
%defattr(-,root,root)
%{pfx}/*

