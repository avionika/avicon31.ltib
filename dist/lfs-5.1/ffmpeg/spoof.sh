#!/bin/sh
export RPM_PACKAGE_NAME=ffmpeg
export PWD=/opt/ltib/rpm/BUILD/ffmpeg-2.5.3
export RPM_BUILD_ROOT=/opt/ltib/tmp/ffmpeg

export LESSKEY=/etc/lesskey.bin
export BUILDLD=/usr/bin/ld
export SYSCFG_TMPFS_SIZE=512k
export SYSCFG_LOGING_TTY="::respawn:/sbin/getty -L ttymxc0 115200 vt100"
export RPM_OPT_FLAGS=-O2
export NNTPSERVER=news
export MANPATH=/usr/lib64/mpi/gcc/openmpi/share/man:/usr/local/man:/usr/local/share/man:/usr/share/man:/opt/kde3/share/man
export AR=ar
export PKG_BUSYBOX_PRECONFIG=busybox.config
export XDG_SESSION_ID=1
export KDE_MULTIHEAD=false
export SSH_AGENT_PID=4134
export BUILDSTRIP=/usr/bin/strip
export PKG_CXX_WANT_SHARED_LIBS=y
export PKG_LIBC_WANT_HEADERS1=y
export HOSTNAME=shpagin
export DM_CONTROL=/var/run/xdmctl
export XKEYSYMDB=/usr/X11R6/lib/X11/XKeysymDB
export PKG_DIRECTFB_WANT_TS=y
export HOST=shpagin
export SHELL=/bin/bash
export TERM=xterm
export PROFILEREAD=true
export XDM_MANAGED=method=classic
export HISTSIZE=1000
export DYNAMIC_LINKER=/lib/ld.so.1
export MAKEFLAGS="-j 8"
export PKG_GDB_SERVER_WANT_ED=y
export RPM_PACKAGE_RELEASE=1
export TMPDIR=/tmp
export KONSOLE_DBUS_SERVICE=:1.75
export GTK2_RC_FILES=/etc/gtk-2.0/gtkrc:/home/ssavchuk/.gtkrc-2.0-kde4:/home/ssavchuk/.gtkrc-2.0-qtengine:/home/ssavchuk/.gtkrc-2.0:/home/ssavchuk/.kde4/share/config/gtkrc-2.0
export TOOLCHAIN_CFLAGS="-O2 -march=armv7-a -mfpu=vfpv3 -mfloat-abi=softfp"
export SYSCFG_LOADKERNELADDR=0x800000
export KONSOLE_PROFILE_NAME=Shell
export SYSCFG_DEPLOYMENT_STYLE=JFFS2
export GS_LIB=/home/ssavchuk/.fonts
export GTK_RC_FILES=/etc/gtk/gtkrc:/home/ssavchuk/.gtkrc:/home/ssavchuk/.kde4/share/config/gtkrc
export PKG_CXX_WANT_HEADERS=y
export MORE=-sl
export WINDOWID=67108890
export BUILDCXX="/usr/bin/g++ -B/usr/bin//"
export OLDPWD=/opt/ltib/rpm/BUILD
export SOFT_FP_ARCH=
export SHELL_SESSION_ID=09b78382b0bc4000b0680ceb7e7346b7
export XSESSION_IS_UP=yes
export PKG_XORG_SERVER_WANT_XORG=y
export SYSCFG_START_INETD=y
export GTK_MODULES=canberra-gtk-module
export KDE_FULL_SESSION=true
export SYSCFG_START_SSHD=y
export RPM_SOURCE_DIR=/opt/ltib/rpm/SOURCES
export JRE_HOME=/usr/lib64/jvm/jre
export USER=ssavchuk
export SYSCFG_RAM_DIRS="/tmp /var"
export LD_LIBRARY_PATH=/usr/lib64/mpi/gcc/openmpi/lib64
export LS_COLORS="no=00:fi=00:di=01;34:ln=00;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=41;33;01:ex=00;32:*.cmd=00;32:*.exe=01;32:*.com=01;32:*.bat=01;32:*.btm=01;32:*.dll=01;32:*.tar=00;31:*.tbz=00;31:*.tgz=00;31:*.rpm=00;31:*.deb=00;31:*.arj=00;31:*.taz=00;31:*.lzh=00;31:*.lzma=00;31:*.zip=00;31:*.zoo=00;31:*.z=00;31:*.Z=00;31:*.gz=00;31:*.bz2=00;31:*.tb2=00;31:*.tz2=00;31:*.tbz2=00;31:*.xz=00;31:*.avi=01;35:*.bmp=01;35:*.fli=01;35:*.gif=01;35:*.jpg=01;35:*.jpeg=01;35:*.mng=01;35:*.mov=01;35:*.mpg=01;35:*.pcx=01;35:*.pbm=01;35:*.pgm=01;35:*.png=01;35:*.ppm=01;35:*.tga=01;35:*.tif=01;35:*.xbm=01;35:*.xpm=01;35:*.dl=01;35:*.gl=01;35:*.wmv=01;35:*.aiff=00;32:*.au=00;32:*.mid=00;32:*.mp3=00;32:*.ogg=00;32:*.voc=00;32:*.wav=00;32:"
export SYSCFG_BAUD=115200
export PKG_LIBC_WANT_STATIC_LIBS=y
export CCACHE_DIR=/home/ssavchuk/.ccache
export PKG_NCURSES_WANT_REDUCED_SET=y
export XNLSPATH=/usr/share/X11/nls
export PKG_U_BOOT_BUILD_ARGS=OPTFLAGS=-Os
export TOOLCHAIN_PATH=/usr/local/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_GNU_Linux
export SSH_AUTH_SOCK=/tmp/ssh-5PCV2dsniiB4/agent.4047
export QEMU_AUDIO_DRV=pa
export HOSTTYPE=x86_64
export CONFIG_DIR=/opt/ltib/config
export DEV_IMAGE=/opt/ltib/rootfs
export CONFIG_SITE=/usr/share/site/x86_64-unknown-linux-gnu
export SESSION_MANAGER=local/shpagin:@/tmp/.ICE-unix/4240,unix/shpagin:/tmp/.ICE-unix/4240
export FROM_HEADER=
export BUILDARCH=x86_64
export BUILDRANLIB=/usr/bin/ranlib
export CFGHOST=arm-linux
export PKG_LIBX11=y
export PAGER=less
export TOP=/opt/ltib
export PLATFORM_PATH=/opt/ltib/config/platform/imx
export PKG_UDEV_WANT_AUTOMOUNT=y
export CSHEDIT=emacs
export BUILDCC="ccache /usr/bin/gcc -B/usr/bin/"
export SYSCFG_BOOT_KERNEL="arch/arm/boot/uImage arch/arm/boot/zImage"
export SYS_WANT_MMU=y
export SYSCFG_CUTARG=zImage
export MC_TMPDIR=/tmp/mc-ssavchuk
export XDG_CONFIG_DIRS=/etc/xdg
export PKG_UDEV_WANT_NON_RAID=y
export MINICOM=-c on
export TOOLCHAIN_PREFIX=arm-none-linux-gnueabi-
export LIBC_HACKING=
export LTIB_BATCH=
export PKG_LIBC_WANT_C_LOCALES=y
export SYS_WANT_SHARED=y
export MAIL=/var/spool/mail/ssavchuk
export PATH=/opt/ltib/bin:/opt/freescale/ltib/usr/spoof:/opt/ltib/bin:/opt/freescale/ltib/usr/bin:/usr/local/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_GNU_Linux/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/X11R6/bin
export DESKTOP_SESSION=default
export PKG_BUSYBOX=y
export CPU=
export SYSCFG_START_PORTMAP=y
export PKG_SKELL_WANT_TERMINFO=y
export JAVA_BINDIR=/usr/lib64/jvm/jre/bin
export QT_IM_MODULE=xim
export LD=ld
export GNUTARCH=arm
export PKG_GCC_WANT_LIBGCC_SHARED=y
export INPUTRC=/home/ssavchuk/.inputrc
export DEFPFX=/opt/freescale/ltib
export JAVA_HOME=/usr/lib64/jvm/jre
export XMODIFIERS=@im=local
export KONSOLE_DBUS_WINDOW=/Windows/1
export KDE_SESSION_UID=1000
export LANG=C
export PKG_AMD_GPU_X11_BIN_BUILD_OPT=y
export PKG_KERNEL_WANT_HEADERS=y
export PYTHONSTARTUP=/etc/pythonstart
export SYSCFG_LOADDTBADDR=0x9F0000
export PKG_LIBC_WANT_CRT_FILES=y
export RPM_PACKAGE_VERSION=1.15
export RPM_OS=linux
export INITTAB_LINE="::respawn:-/sbin/getty -L console 0 screen"
export KONSOLE_DBUS_SESSION=/Sessions/3
export PLATFORM=imx51
export PKG_QT_PHONON=
export PKG_DIRECTFB=y
export SYSCFG_CONSOLEDEV=ttyS0
export HISTCONTROL=ignoreboth
export SSH_ASKPASS=/usr/lib/ssh/ssh-askpass
export CXX=g++
export LINTARCH=arm
export RPM_DOC_DIR=/opt/freescale/ltib/usr/doc
export AUDIODRIVER=pulseaudio
export GPG_TTY=/dev/pts/5
export HOME=/home/ssavchuk
export XDG_SEAT=seat0
export SHLVL=5
export QT_SYSTEM_DIR=/usr/share/desktop-data
export COLORFGBG="15;0"
export TOOLCHAIN=
export LESS_ADVANCED_PREPROCESSOR=no
export LANGUAGE=
export SDL_AUDIODRIVER=pulse
export ALSA_CONFIG_PATH=/etc/alsa-pulse.conf
export KDE_SESSION_VERSION=4
export OSTYPE=linux
export SYSCFG_BOOTLOADER=u-boot
export XCURSOR_THEME=Oxygen_White
export LS_OPTIONS="-N --color=tty -T 0"
export TOOLCHAIN_TYPE=
export UCLIBC=
export MC_SID=6539
export WINDOWMANAGER=/usr/bin/startkde
export ENDIAN=little
export PKG_U_BOOT_CONFIG_TYPE=mx51_bbg_config
export PKG_OPENSSH_WANT_HACKABLE_KEYS=y
export SYSCFG_HOSTNAME=freescale
export SYSCFG_WANT_LOGIN_TTY=y
export MACHTYPE=x86_64-suse-linux
export LOGNAME=ssavchuk
export G_FILENAME_ENCODING=@locale,UTF-8,ISO-8859-15,CP1252
export LESS="-M -I -R"
export CVS_RSH=ssh
export PKG_SYSVINIT=
export XDG_DATA_DIRS=/usr/share:/usr/local/share:/usr/share:/etc/opt/kde3/share:/opt/kde3/share
export DBUS_SESSION_BUS_ADDRESS=unix:abstract=/tmp/dbus-ty7kGMIeWi,guid=532598e07f937f89daf15cff52e0aad5
export PKG_LIBC_WANT_SHARED_LIBS=y
export RPM_BUILD_DIR=/opt/ltib/rpm/BUILD
export LESSOPEN="lessopen.sh %s"
export PKG_CONFIG_PATH=/opt/ltib/rootfs/usr/lib/pkgconfig
export GLIBC_WANT_KERNEL_HEADERS=
export SYSCFG_RUNKERNELADDR=0x0
export PKG_UDEV_WANT_IMX=y
export USE_FAM=
export WINDOWPATH=7
export PKG_BOOT_STREAM_CMDLINE4=
export SYSCFG_KTARG=uImage
export RPM_ARCH=unknown
export DISPLAY=:0
export XDG_RUNTIME_DIR=/run/user/1000
export PROFILEHOME=
export CROSS_COMPILE=
export UNSPOOF_PATH=/opt/ltib/bin:/opt/freescale/ltib/usr/bin:/usr/local/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_GNU_Linux/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/X11R6/bin
export CC=gcc
export QT_PLUGIN_PATH=/home/ssavchuk/.kde4/lib64/kde4/plugins/:/usr/lib64/kde4/plugins/
export KCONFIG_NOTIMESTAMP=no
export BUILDCPP=/usr/bin/cpp
export SYSCFG_START_UDEV=y
export XAUTHLOCALHOSTNAME=shpagin
export GTK_IM_MODULE=cedilla
export XDG_CURRENT_DESKTOP=KDE
export PKG_BOOT_STREAM_CMDLINE1=
export SYSCFG_TMPFS=tmpfs
export LESSCLOSE="lessclose.sh %s %s"
export PKG_CXX_WANT_STATIC_LIBS=y
export G_BROKEN_FILENAMES=1
export QT_IM_SWITCHER=imsw-multi
export PKG_BOOT_STREAM_CMDLINE3=
export PKG_KERNEL_PRECONFIG=imx5_defconfig
export JAVA_ROOT=/usr/lib64/jvm/jre
export COLORTERM=1
export SPOOF_PATH=/opt/ltib/bin:/opt/freescale/ltib/usr/spoof:/opt/ltib/bin:/opt/freescale/ltib/usr/bin:/usr/local/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_GNU_Linux/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/X11R6/bin
export PKG_BOOT_STREAM_CMDLINE2=
export SYSCFG_LOADRAMADDR=0xB00000
export prefix=/usr
export _build=x86_64-unknown-linux-gnu

case "$1" in
    configure)
	./configure --arch=$LINTARCH --target-os=linux --disable-yasm --disable-altivec --disable-mmx --enable-cross-compile --prefix=${_prefix} --mandir=${_mandir} --extra-cflags=-I../linux/include --enable-shared --disable-static
	;;
    make)
	CPU_CORES_COUNT=1
	if [ -a /proc/cpuinfo ]; then
	    CPU_CORES_COUNT=$(expr $(grep 'processor' /proc/cpuinfo | wc -l) + 1)
	fi
	echo "CPU_CORES_COUNT=$CPU_CORES_COUNT"
	make -j $CPU_CORES_COUNT && echo '-- build successful'
	;;
    install)
	rm -rf $RPM_BUILD_ROOT
	target_cpu=arm
	pfx=/opt/freescale/rootfs/${_target_cpu}
	make install DESTDIR=$RPM_BUILD_ROOT/${pfx}
	;;
    *)
	echo "Usage: $0 {configure|make|install}"
	exit 1
	;;
esac
