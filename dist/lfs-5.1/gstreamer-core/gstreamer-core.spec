%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : GStreamer Core
Name            : gstreamer-core
Version         : 0.10.36
Release         : 2
License         : LGPL
Vendor          : Freescale Semiconductor
Packager        : Kurt Mahan
Group           : Applications/System
Source          : gstreamer-%{version}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
URL             : http://gstreamer.freedesktop.org/src/gstreamer/gstreamer-%{version}

%Description
%{summary}

%Prep
%setup -n gstreamer-%{version}

%Build
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build} \
            --disable-valgrind --without-check NM=nm
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}
find $RPM_BUILD_ROOT/%{pfx}/%{_prefix}/lib/ -name "*.la" | xargs rm -f

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
