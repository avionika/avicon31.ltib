%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : Dialog is a utility to create nice user interfaces to shell scripts
Name            : dialog
Version         : 1.2
Release         : 20140112
License         : LGPL-2.1+
Vendor          : Freescale
Packager        : Sergey Savchuk
Group           : Development/Libraries/Other
URL             : ftp://ftp.us.debian.org/debian/pool/main/d/dialog/dialog_1.2-20140112.orig.tar.gz
Source          : %{name}_%{version}-%{release}.orig.tar.gz
Patch0          : dialog-1.2-20140112.cross_comp.patch
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

%Prep
%setup -n %{name}-%{version}-%{release}
%patch0 -p1

%Build
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build}
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
