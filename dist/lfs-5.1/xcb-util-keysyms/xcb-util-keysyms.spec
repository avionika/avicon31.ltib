%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : XCB util-keysyms
Name            : xcb-util-keysyms
Version         : 0.3.9
Release         : 1
License         : X11
Vendor          : X.Org Foundation
Packager        : Sergey Savchuk <serg.malinovski@gmail.com>
Group           : System Environment/Libraries
URL             : http://www.x.org/
Source          : %{name}-%{version}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
X.Org XCB util modules.

%Prep
%setup

%Build
./configure --prefix=%{_prefix} --host=$CFGHOST --build=%{_build}
make

%Install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*

