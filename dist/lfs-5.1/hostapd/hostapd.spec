%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : User space daemon for access poin
Name            : hostapd
Version         : 1.1
Release         : 1
License         : GPL
Vendor          : Radioavionika
Packager        : Sergey Savchuk
Group           : Application/System
Source          : %{name}-%{version}.tar.gz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
User space daemon for access points, including, e.g., IEEE 802.1X/WPA/EAP
Authenticator for number of Linux and BSD drivers, RADIUS client,
integrated EAP server, and RADIUS authentication server

%{summary}

%Prep
%setup 

%Build
cd hostapd
#Create .config file for build
cat > .config << EOF
CONFIG_EAP_PSK=y
CONFIG_EAP=y
CONFIG_CTRL_IFACE=y
CONFIG_L2_PACKET=linux
CONFIG_DRIVER_NL80211=y
CONFIG_DRIVER_HOSTAP=y
CONFIG_IAPP=y
CONFIG_RSN_PREAUTH=y
CONFIG_PEERKEY=y
CONFIG_EAP_MD5=y
CONFIG_EAP_TLS=y
CONFIG_EAP_MSCHAPV2=y
CONFIG_EAP_PEAP=y
CONFIG_EAP_GTC=y
CONFIG_EAP_TTLS=y
EOF

make CONFIG_L2_PACKET=linux

%Install
rm -rf $RPM_BUILD_ROOT
cd hostapd
make install CONFIG_L2_PACKET=linux prefix=%{_prefix} DESTDIR=$RPM_BUILD_ROOT/%{pfx}
mkdir -p  $RPM_BUILD_ROOT/%{pfx}/usr/etc/unifi

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
