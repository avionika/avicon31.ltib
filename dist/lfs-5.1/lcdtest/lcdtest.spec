%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : LCD monitor test pattern generator
Name            : lcdtest
Version         : 1.18
Release         : 1
Source          : http://www.brouhaha.com/~eric/software/lcdtest/download/%{name}-%{version}.tar.gz
URL             : http://www.brouhaha.com
License         : GPLv3
Group           : Applications/Text
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}


%Description
lcdtest is a utility to display LCD monitor test patterns.
It may be useful for adjusting the pixel clock frequency and phase on LCD monitors
when using analog inputs, and for finding pixels that are stuck on or off.
lcdtest uses the SDL library, and is known to work on Linux and Windows.

%Prep
%setup

%Build
if [ ! -d build ]; then
    mkdir build
fi
gcc -o build/lcdtest.o -c -g -Wall -Wextra -DRELEASE=1.18 src/lcdtest.c --sysroot=${DEV_IMAGE}
gcc -o build/lcdtest build/lcdtest.o -lSDL -lSDL_image -lSDL_ttf --sysroot=${DEV_IMAGE}

%Install
rm -rf $RPM_BUILD_ROOT
mkdir -p -- $RPM_BUILD_ROOT/%{pfx}/usr/bin/
/usr/bin/install -c -m 755 build/lcdtest $RPM_BUILD_ROOT/%{pfx}/usr/bin/lcdtest

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*

