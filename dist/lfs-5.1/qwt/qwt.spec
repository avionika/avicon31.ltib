%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : qwt
Name            : qwt
Version         : 6.1.2.qreal
Release         : 0
License         : GNU GPL
Vendor          : Freescale
Packager        : Sergey Savchuk
Group           : System Environment/Libraries
Source          : %{name}-%{version}.tar.bz2
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}
URL             : http://sourceforge.net/projects/qwt/files/qwt/6.1.2/qwt-6.1.2.tar.bz2/download
%Description
%{summary}

%Prep
%setup

%Build

/usr/local/qt-5.4.0-host/bin/qmake qwt.pro

CPU_CORES_COUNT=1
if [ -a /proc/cpuinfo ]; then
    CPU_CORES_COUNT=$(expr $(grep 'processor' /proc/cpuinfo | wc -l) + 1)
fi
echo "CPU_CORES_COUNT=$CPU_CORES_COUNT"

make -j $CPU_CORES_COUNT && echo '-- build successful'

%Install

make install INSTALL_ROOT=$RPM_BUILD_ROOT%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
